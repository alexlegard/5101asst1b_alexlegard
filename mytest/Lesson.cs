﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mytest
{
    public class Lesson
    {
        public int time;
        public String type;
        public String location;
        public List<String> dayOfWeek;

        //CONSTRUCTOR
        public Lesson(int lessonTime, String lessonType, String lessonLocation, List<String> lessonDayOfWeek)
        {
            time = lessonTime;
            type = lessonType;
            location = lessonLocation;
            dayOfWeek = lessonDayOfWeek;
        }

        public String info()
        {
            Console.WriteLine("Time:" + time);
            Console.WriteLine("Lesson type:" + type);
            Console.WriteLine("Location:" + location);
            Console.WriteLine("Day of week:" + dayOfWeek);

            String output = "Time: " + time;
            output += "Type: " + type;
            output += "Location: " + location;
            output += "dayOfWeek: " + dayOfWeek;

            return output;
        }
    }
}