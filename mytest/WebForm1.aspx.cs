﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace mytest
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void ButtonClick(object sender, EventArgs e)
        {
            Console.WriteLine("Hello World!");

            /*Some simple page validation; If the page isn't valid, return immediately*/
            if (!Page.IsValid)
            {
                return;
            }

            /*Get the information from the .cs file by initialising variables with the correct data type.
             * Start by creating the Client object
             * then the Lesson object
             * then the Request object to encapsulate both*/

            /*CLIENT
             * 
             * Variables: name (string), age (int), phone (string), email (string), medical conditions (List<String>),
             * experience (String)*/

            String name = clientName.Text.ToString(); /*This is how to get the value from a text box*/
            int age = int.Parse(clientAge.Text); /*Age is another text box which we parse for an int*/
            String phone = clientPhone.Text.ToString();/*Another text box*/
            String email = clientEmail.Text.ToString();/*Another text box*/
            List<String> conditionList = new List<String>() {}; /*User chooses medical conditions from checkboxes*/

            foreach (Control control in medicalConditions.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox condition = (CheckBox)control;
                    if (condition.Checked)
                    {
                        conditionList.Add(condition.Text);
                    }

                }
            }
            String experience = clientExperience.SelectedItem.Value.ToString();/*More checkboxes*/

            Client client = new Client(name, age, phone, email, conditionList, experience);

            /*LESSON
             * 
             * Variables: Time (int), lesson type (String), location (String), day of week (List<string>)*/

            int time = int.Parse(lessonTime.Text);
            String type = lessonType.SelectedItem.Value.ToString();
            String location = facility.SelectedItem.Value.ToString();
            List<string> days = new List<string>() { };

            foreach (Control control in lessonDays.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox day = (CheckBox)control;
                    if (day.Checked)
                    {
                        days.Add(day.Text);
                    }

                }
            }

            Lesson lesson = new Lesson(time, type, location, days);

            /*REQUEST
             * 
             * Variables: client (client), lesson (lesson)*/

            Request request = new Request(client, lesson);

            /*Output some information about the client and lesson that exist within the Request object.*/
            Console.WriteLine(request.clientInfo());
            Console.WriteLine(request.lessonInfo());
        }
    }
}