﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="mytest.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Swimming lessons registration</h1>
            <!--Client- Name-->
            <div>
                <asp:TextBox runat="server" ID="clientName" placeholder="Name"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Name" ControlToValidate="clientName" ID="validatorName"></asp:RequiredFieldValidator>
            </div>
            <div>
                <!--Client- Age-->
                <asp:TextBox runat="server" ID="clientAge" placeholder="Age"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an age" ControlToValidate="clientAge" ID="validatorAge"></asp:RequiredFieldValidator>
                <asp:CompareValidator runat="server" ControlToValidate="clientAge" Type="String" Operator="GreaterThan" ValueToCompare="0" ErrorMessage="Age is invalid."></asp:CompareValidator>
            </div>
            <!--Client- Phone-->
            <div>
                <asp:TextBox runat="server" ID="clientPhone" placeholder="Phone"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a phone number" ControlToValidate="clientPhone" ID="validatorPhone"></asp:RequiredFieldValidator>
            </div>
            <!--Client- Email-->
            <div>
                <asp:TextBox runat="server" ID="clientEmail" placeholder="Email"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a valid email address" ControlToValidate="clientEmail" ID="RequiredFieldValidator1"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div id="medicalConditions" runat="server"><!-- Client- Medical conditions-->
            <h4>Medical conditions</h4>
            <asp:CheckBox runat="server" ID="condition1" Text="Heart disease" />
            <asp:CheckBox runat="server" ID="condition2" Text="Hard of breathing" />
            <asp:CheckBox runat="server" ID="condition3" Text="Cancer" />
        </div>
        <div><!--Client-Experience-->
            <h4>Level of experience</h4>
            <asp:RadioButtonList runat="server" ID="clientExperience">
                <asp:ListItem runat="server" ID="isBeginner" Text="Beginner" GroupName="skillLevel" />
                <asp:ListItem runat="server" ID="isIntermediate" Text="Intermediate" GroupName="skillLevel" />
                <asp:ListItem runat="server" ID="isAdvanced" Text="Advanced" GroupName="skillLevel" />
            </asp:RadioButtonList>
        </div>
        <!--Lesson-Time-->
        <div>
            <h4>Preferred time</h4> 
            <asp:DropDownList runat="server" ID="lessonTime">
                <asp:ListItem Value="9" Text="9:00"></asp:ListItem>
                <asp:ListItem Value="10" Text="10:00"></asp:ListItem>
                <asp:ListItem Value="11" Text="11:00"></asp:ListItem>
                <asp:ListItem Value="12" Text="12:00"></asp:ListItem>
                <asp:ListItem Value="1" Text="1:00"></asp:ListItem>
                <asp:ListItem Value="2" Text="2:00"></asp:ListItem>
                <asp:ListItem Value="3" Text="3:00"></asp:ListItem>
                <asp:ListItem Value="4" Text="4:00"></asp:ListItem>
                <asp:ListItem Value="5" Text="5:00"></asp:ListItem>
                <asp:ListItem Value="6" Text="6:00"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div><!--Private/group radio buttons-->
            <h4>Group size</h4>
            <asp:RadioButtonList runat="server" ID="lessonType">
                <asp:ListItem runat="server" Text="Private" GroupName="type" />
                <asp:ListItem runat="server" Text="Group" GroupName="type" />
            </asp:RadioButtonList>
        </div>
        <div><!--Location radio buttons-->
            <h4>Location</h4>
            <asp:RadioButtonList runat="server" ID="facility">
                <asp:ListItem runat="server" Text="Yellow Desert Pool" GroupName="facility" />
                <asp:ListItem runat="server" Text="Blue Mountain Pool" GroupName="facility" />
                <asp:ListItem runat="server" Text="Green Forest Pool" GroupName="facility" />
            </asp:RadioButtonList>
        </div>
        <div id="lessonDays" runat="server">
            <!--Day of week check box-->
            <h4>Day of week</h4>
            <asp:CheckBox runat="server" ID="lessonMon" Text="Monday" groupName="dayOfWeek"/>
            <asp:CheckBox runat="server" ID="lessonTues" Text="Tuesday" groupName="dayOfWeek"/>
            <asp:CheckBox runat="server" ID="lessonWed" Text="Wednesday" groupName="dayOfWeek"/>
            <asp:CheckBox runat="server" ID="lessonThurs" Text="Thursday" groupName="dayOfWeek"/>
            <asp:CheckBox runat="server" ID="lessonFri" Text="Friday" groupName="dayOfWeek"/>
        </div>

        <div>
            </br>
            <asp:Button runat="server" ID="myButton" Text="Submit" OnClick="ButtonClick" />
        </div>
        <div id="res" runat="server"></div>
    </form>
    <p runat="server"></p>
</body>
</html>
