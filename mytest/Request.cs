﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mytest
{
    public class Request
    {
        public Client cli;
        public Lesson les;

        public Request(Client client, Lesson lesson)
        {
            cli = client;
            les = lesson;
        }

        public String info()
        {
            Console.WriteLine("Name:" + cli.name);
            Console.WriteLine("Age:" + cli.age);
            Console.WriteLine("Phone:" + cli.phone);
            Console.WriteLine("Email:" + cli.email);
            Console.WriteLine("Medical conditions:" + cli.medicalConditions);
            Console.WriteLine("Experience:" + cli.experience);
            Console.WriteLine("Time:" + les.time);
            Console.WriteLine("Lesson type:" + les.type);
            Console.WriteLine("Location:" + les.location);
            Console.WriteLine("Day of week:" + les.dayOfWeek);

            String output = "Name: " + cli.name;
            output += "Age: " + cli.age;
            output += "Phone number: " + cli.phone;
            output += "Email: " + cli.email;
            output += "Medical conditions: " + cli.medicalConditions;
            output += "Experience: " + cli.experience;
            output += "Time: " + les.time;
            output += "Lesson type: " + les.type;
            output += "Location: " + les.location;
            output += "Day of week: " + les.dayOfWeek;

            return output;
        }

        public int price()
        {

            if(cli.experience == "beginner")
            {
                return 150;
            } else if (cli.experience == "intermediate")
            {
                return 200;
            } else if (cli.experience == "advanced")
            {
                return 250;
            }
            return 0;
        }

        public string clientInfo()
        {
            String output = cli.info();
            return output;
        }

        public string lessonInfo()
        {
            String output = les.info();
            return output;
        }
    }
}