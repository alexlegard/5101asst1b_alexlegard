﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mytest
{
    public class Client
    {
        public String name;
        public int age;
        public String phone;
        public String email;
        public List<String> medicalConditions;
        public String experience;

        //ADD A CONSTRUCTOR
        public Client(String clientName, int clientAge, String clientPhone, String clientEmail, List<String> clientMedicalConditions, String clientExperience)
        {
            name = clientName;
            age = clientAge;
            phone = clientPhone;
            email = clientEmail;
            medicalConditions = clientMedicalConditions;
            experience = clientExperience;
        }

        static void info()
        {
            Console.WriteLine("Name:");
            Console.WriteLine("Age:");
            Console.WriteLine("Phone:");
            Console.WriteLine("Email:");
            Console.WriteLine("Medical conditions:");
            Console.WriteLine("Experience:");
        }
    }
}